import 'package:flutter/material.dart';
import 'package:warehouse/src/barangKeluar/menuBarangKeluar.dart';
import 'package:warehouse/src/barangMasuk/menuBarangMasuk.dart';
import 'package:warehouse/src/login/loginPage.dart';
import 'package:warehouse/src/menu/menu.dart';
import 'package:warehouse/src/menu/pilihGudang.dart';

class route extends StatelessWidget {
  const route({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Warehouse',
      // Start the app with the "/" named route. In this case, the app starts
      // on the FirstScreen widget.
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const LoginPage(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/pilihGudang': (context) => const pilihGudang(),
        '/mainMenu': (context) => const mainMenu(),
        '/barangMasuk': (context) => const barangMasuk(),
        '/barangKeluar': (context) => const barangKeluar(),
      },
    );
  }
}
