import 'package:flutter/material.dart';

class mainMenu extends StatefulWidget {
  const mainMenu({Key? key}) : super(key: key);

  @override
  State<mainMenu> createState() => _mainMenuState();
}

class _mainMenuState extends State<mainMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: Text("Menu Utama - Gudang A"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 3,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/barangMasuk');
                      },
                      child: Card(
                        color: Colors.white,
                        elevation: 10,
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(
                              Icons.input_outlined,
                              size: 35,
                              color: Colors.blue,
                            ),
                            Text(
                              "Scan Barang Masuk",
                              style: TextStyle(fontSize: 15),
                            )
                          ],
                        )),
                      ),
                    )),
                Flexible(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/barangKeluar');
                      },
                      child: Card(
                        color: Colors.white,
                        elevation: 10,
                        child: Center(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(
                              Icons.input_outlined,
                              size: 35,
                              color: Colors.red,
                            ),
                            Text(
                              "Scan Barang Keluar",
                              style: TextStyle(fontSize: 15),
                            )
                          ],
                        )),
                      ),
                    )),
              ],
            ),
          ),
          Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(3),
                color: Color.fromARGB(255, 244, 244, 249),
                child: Card(
                  color: Colors.purple,
                  elevation: 5,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.compare_arrows,
                          color: Colors.white,
                        ),
                        Text(
                          '   History Flow Barang',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          Flexible(
            flex: 10,
            child: Container(
              margin: EdgeInsets.all(3),
              color: Color.fromARGB(255, 244, 244, 249),
              child: ListView(
                children: <Widget>[
                  cardActivity(
                      'Barang Masuk', 5414750, 'Celana Bahan Tjap Ngatoeng', 5),
                  cardActivity('Barang Masuk', 5425402, 'Topi Copet', 2),
                  cardActivity('Barang Masuk', 1772701, 'Rok Karoeng Goni', 1),
                  cardActivity(
                      'Barang Keluar', 1377902, 'Kemeja Kotak-Kotak Serbet', 4),
                  cardActivity(
                      'Barang Keluar', 9298392, 'Cincin Slank Air Flow', 15),
                  cardActivity(
                      'Barang Keluar', 8827538, 'Kalung Tambang Pasung', 2),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Card cardActivity(String deskripsi, int sku, String namaBarang, int qty) {
    return Card(
      color: Colors.orangeAccent,
      elevation: 10,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 7, 10, 7),
                child: Text(
                  qty.toString() + " " + deskripsi,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
          Container(
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  'SKU ' + sku.toString(),
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                                Text(
                                  namaBarang,
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
