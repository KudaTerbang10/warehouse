import 'package:flutter/material.dart';

class pilihGudang extends StatefulWidget {
  const pilihGudang({super.key});

  @override
  State<pilihGudang> createState() => _pilihGudangState();
}

class _pilihGudangState extends State<pilihGudang> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 244, 244, 249),
        appBar: AppBar(
          title: Text("Pilih Gudang"),
          backgroundColor: Colors.orangeAccent,
        ),
        body: Container(
          margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
          child: ListView(
            children: [
              GestureDetector(
                child: CardGudang('Gudang A', 'Bandung'),
                onTap: () {
                  Navigator.pushNamed(context, '/mainMenu');
                },
              ),
              CardGudang('Gudang B', 'Jakarta'),
            ],
          ),
        ));
  }

  Container CardGudang(String name, String alamat) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Card(
        color: Colors.orangeAccent,
        elevation: 10,
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(10, 7, 10, 7),
                  child: Text(
                    name,
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
            Container(
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                alamat,
                                style: TextStyle(fontSize: 30),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
