import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:warehouse/src/menu/pilihGudang.dart';
import 'package:warehouse/src/Routing/route.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  double getSmallDiameter(BuildContext context) =>
      MediaQuery.of(context).size.width * 2 / 3;

  double getBigDiameter(BuildContext context) =>
      MediaQuery.of(context).size.width * 7 / 8;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 246, 217, 242),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: -getSmallDiameter(context) / 3,
            right: -getSmallDiameter(context) / 3,
            child: newCircle(
                getSmallDiameter(context),
                getSmallDiameter(context),
                155,
                239,
                50,
                217,
                155,
                137,
                255,
                253,
                ''),
          ),
          Positioned(
            top: -getSmallDiameter(context) / 4,
            left: -getSmallDiameter(context) / 4,
            child: newCircle(getBigDiameter(context), getBigDiameter(context),
                155, 52, 148, 230, 155, 236, 110, 173, 'Login'),
          ),
          Positioned(
            right: -getBigDiameter(context) / 2,
            bottom: -getBigDiameter(context) / 2,
            child: newCircle(getBigDiameter(context), getBigDiameter(context),
                50, 52, 148, 230, 50, 236, 110, 173, ''),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: Colors.blueGrey,
                    ),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  margin: EdgeInsets.fromLTRB(30, 250, 30, 15),
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 25),
                  child: Column(
                    children: <Widget>[
                      newField('Username', Icons.person, false),
                      newField('Password', Icons.lock, true),
                    ],
                  ),
                ),
                // Text(
                //   'Forgot Password?',
                //   textAlign: TextAlign.center,
                //   style: TextStyle(
                //       color: Colors.blue, fontWeight: FontWeight.w500),
                // ),
                Container(
                    margin: EdgeInsets.fromLTRB(30, 15, 30, 15),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Colors.deepOrange,
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 20)),
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, '/pilihGudang');
                        },
                        child: Text(
                          'Sign In',
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ))),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Developed By ', style: TextStyle(color: Colors.grey)),
                    Text(
                      'PT Bartech Media Solusi',
                      style: TextStyle(color: Colors.blue),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  TextField newField(String text, IconData iconData, bool obscure) {
    return TextField(
      obscureText: obscure,
      style: TextStyle(color: Colors.blue, fontSize: 20),
      decoration: InputDecoration(
          label: Text(text),
          prefix: Container(
              margin: EdgeInsets.all(10),
              child: Icon(
                iconData,
                color: Colors.blue,
              ))),
    );
  }

  Container newCircle(double height, double width, int a, int r, int g, int b,
      int w, int x, int y, int z, String text) {
    return Container(
      child: Center(
          child: Text(
        text,
        style: TextStyle(
            fontFamily: 'amsterdam', fontSize: 40, color: Colors.white),
      )),
      width: width,
      height: height,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(colors: <Color>[
            Color.fromARGB(a, r, g, b),
            Color.fromARGB(w, x, y, z),
          ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
    );
  }
}
