import 'package:flutter/material.dart';

class barangKeluar extends StatefulWidget {
  const barangKeluar({Key? key}) : super(key: key);

  @override
  State<barangKeluar> createState() => barangKeluarState();
}

class barangKeluarState extends State<barangKeluar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: Text("Barang Keluar - Gudang A"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 3,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Card(
                      color: Colors.white,
                      elevation: 5,
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.attach_money,
                            size: 35,
                            color: Colors.red,
                          ),
                          Text(
                            "Penjualan",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                    )),
                Flexible(
                    flex: 1,
                    child: Card(
                      color: Colors.white,
                      elevation: 5,
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.change_circle,
                            size: 35,
                            color: Colors.red,
                          ),
                          Text(
                            "Mutasi Stock Keluar",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                    )),
              ],
            ),
          ),
          Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(3),
                color: Color.fromARGB(255, 244, 244, 249),
                child: Card(
                  color: Colors.red,
                  elevation: 5,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.output_outlined,
                          color: Colors.white,
                        ),
                        Text(
                          '   History Barang Keluar',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          Flexible(
            flex: 10,
            child: Container(
              margin: EdgeInsets.all(3),
              color: Color.fromARGB(255, 244, 244, 249),
              child: ListView(
                children: <Widget>[
                  cardActivity(
                      'Penjualan', 5414750, 'Kemeja Kotak-Kotak Serbet', 4),
                  cardActivity('Mutasi Stock Keluar', 5425402,
                      'Cincin Slank Air Flow', 15),
                  cardActivity(
                      'Penjualan', 1772701, 'Kalung Tambang Pasung', 2),
                  cardActivity('Penjualan', 1377902, 'Badjoe Punksee Hitam', 4),
                  cardActivity('Penjualan', 9298392, 'Sepatu Jenggel', 15),
                  cardActivity('Penjualan', 8827538, 'Rompi Punk', 2),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Card cardActivity(String deskripsi, int sku, String namaBarang, int qty) {
    return Card(
      color: Colors.orangeAccent,
      elevation: 5,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 7, 10, 7),
                child: Text(
                  deskripsi,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
          Container(
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  'SKU ' + sku.toString(),
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                                Text(
                                  namaBarang,
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
