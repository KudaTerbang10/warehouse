import 'package:flutter/material.dart';

class barangMasuk extends StatefulWidget {
  const barangMasuk({Key? key}) : super(key: key);

  @override
  State<barangMasuk> createState() => barangMasukState();
}

class barangMasukState extends State<barangMasuk> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        title: Text("Barang Masuk - Gudang A"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 3,
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Card(
                      color: Colors.white,
                      elevation: 10,
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.inventory,
                            size: 35,
                            color: Colors.blue,
                          ),
                          Text(
                            "Pembelian",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                    )),
                Flexible(
                    flex: 1,
                    child: Card(
                      color: Colors.white,
                      elevation: 10,
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            Icons.change_circle,
                            size: 35,
                            color: Colors.blue,
                          ),
                          Text(
                            "Mutasi Stock Masuk",
                            style: TextStyle(fontSize: 15),
                          )
                        ],
                      )),
                    )),
              ],
            ),
          ),
          Flexible(
              flex: 1,
              child: Container(
                margin: EdgeInsets.all(3),
                color: Color.fromARGB(255, 244, 244, 249),
                child: Card(
                  color: Colors.blue,
                  elevation: 5,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.input_outlined,
                          color: Colors.white,
                        ),
                        Text(
                          '   History Barang Masuk',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          Flexible(
            flex: 10,
            child: Container(
              margin: EdgeInsets.all(3),
              color: Color.fromARGB(255, 244, 244, 249),
              child: ListView(
                children: <Widget>[
                  cardActivity(
                      'Pembelian', 5414750, 'Celana Bahan Tjap Ngatoeng', 5),
                  cardActivity('Mutasi Stock Masuk', 5425402, 'Topi Copet', 2),
                  cardActivity('Pembelian', 1772701, 'Rok Karoeng Goni', 1),
                  cardActivity('Pembelian', 1377902, 'Badjoe Punksee Hitam', 4),
                  cardActivity('Pembelian', 9298392, 'Sepatu Jenggel', 15),
                  cardActivity('Pembelian', 8827538, 'Rompi Punk', 2),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Card cardActivity(String deskripsi, int sku, String namaBarang, int qty) {
    return Card(
      color: Colors.orangeAccent,
      elevation: 10,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 7, 10, 7),
                child: Text(
                  qty.toString() + " " + deskripsi,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
          Container(
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  'SKU ' + sku.toString(),
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                                Text(
                                  namaBarang,
                                  style: TextStyle(fontSize: 15),
                                  textAlign: TextAlign.right,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
